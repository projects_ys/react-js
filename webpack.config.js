module.exports = {
  context: __dirname,
  entry: './dev/index.js',
  module:{
  	loaders:[
  		{	
	  		test: /\.jsx?$/,
	  		exclude: /(node_modules|bower_components)/,
	  		loader: 'babel_loader',
	  		query: {
	  			presets: ['react', 'es2015', 'stage-0'],
	  			plugins: ['react-html-attrs','transform-class-properties','transform-decorators-legacy'],
	  		}
	  	}
  	]
  },
  output: {
    filename: 'app/scripts.min.js',
  }
};